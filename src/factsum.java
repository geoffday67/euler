import java.math.BigInteger;

public class factsum
{
	private static BigInteger factorial (long n)
	{
		BigInteger f = BigInteger.valueOf (n);
		
		while (--n > 1)
		{
			f = f.multiply(BigInteger.valueOf(n));
		}
		
		return f;
	}
	
	private static long sum_digits (BigInteger n)
	{
		long sum = 0;
		
		while (n.compareTo(BigInteger.ZERO) > 0)
		{
			BigInteger[] dr = n.divideAndRemainder(BigInteger.TEN);
			
			n = dr[0];
			sum += dr[1].longValue();
		}
		
		return sum;
	}
	
	public static void main(String[] args)
	{
		System.out.println(sum_digits(factorial(100)));
	}

}
