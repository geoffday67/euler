package poker;

import java.util.ArrayList;
import java.util.Collections;

/*
 * Represents the total number of cards of each value
 */
public class Summary
{
	private ArrayList<Count> Counts;
	public ArrayList<Count> getCounts()
	{
		return Counts;
	}

	public Summary()
	{
		Counts = new ArrayList<Count>();
	}
	
	public int getValue (int index)
	{
		if (index >= Counts.size())
			return 0;
		
		return Counts.get(index).getValue();
	}
	
	public int getTotal (int index)
	{
		if (index >= Counts.size())
			return 0;
		
		return Counts.get(index).getTotal();
	}
	
	/*
	 * Increment the total for a value. Create a new Count object if needed.
	 */
	public void increment (int value)
	{
		// Find Count object for the specified value and update it
		for (Count count: Counts)
			if (count.getValue() == value)
			{
				count.increment();
				return;
			}
		
		// Existing Count not found so make a new one
		Counts.add (new Count(value));
	}
	
	public void sort()
	{
		// Sort by total, then by value. Uses the Comparable interface of Count
		Collections.sort (Counts);
	}
	
	@Override
	public String toString()
	{
		String result = "";
		
		for (Count count: Counts)
		{
			if (result.length() > 0)
				result += ", ";
			
			result += String.format("%d of %d", count.getTotal(), count.getValue());
		}
		
		return result;
	}
}
