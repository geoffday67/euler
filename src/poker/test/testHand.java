package poker.test;

import static org.junit.Assert.*;
import org.junit.Test;
import poker.Card;
import poker.Card.Suit;
import poker.Hand;
import poker.Summary;

public class testHand
{

	@Test
	public void testHasCardSuit()
	{
		Hand hand = new Hand ("8C TS KC 9H 4S");
		
		assertTrue ("Has a Club", hand.hasCard(Suit.CLUBS));
		assertTrue ("Has a Spade", hand.hasCard(Suit.SPADES));
		assertTrue ("Has a Heart", hand.hasCard(Suit.HEARTS));
		assertTrue ("Has no Diamond", !hand.hasCard(Suit.DIAMONDS));
	}

	@Test
	public void testHasCardInt()
	{
		Hand hand = new Hand ("8C TS KC 9H 4S");
		
		assertTrue ("Has an eight", hand.hasCard(8));
		assertTrue ("Has a ten", hand.hasCard(10));
		assertTrue ("Has an King", hand.hasCard(Card.KING));
		assertTrue ("Has a nine", hand.hasCard(9));
		assertTrue ("Has a four", hand.hasCard(4));
		assertTrue ("Has no Ace", !hand.hasCard(Card.ACE));
	}

	@Test
	public void testHasStraight()
	{
		Hand hand;
		
		hand = new Hand ("2H 3S 6D 5H 4C");
		assertTrue ("Has a straight", hand.hasStraight());
		
		hand = new Hand ("2H 3S AD 5H 4C");
		assertTrue ("Has a straight", hand.hasStraight());
		
		hand = new Hand ("QH TS AD JH KC");
		assertTrue ("Has a straight", hand.hasStraight());
		
		hand = new Hand ("2H AS AD 5H 4C");
		assertTrue ("Has no straight", !hand.hasStraight());
	}
	
	@Test
	public void testHasFlush()
	{
		Hand hand;
		
		hand = new Hand ("2H AH AH 5H 4H");
		assertTrue ("Has flush", hand.hasFlush());
		
		hand = new Hand ("2H AC AH 5H 4H");
		assertTrue ("Has no flush", !hand.hasFlush());
	}
	
	@Test
	public void testSummary()
	{
		Hand hand;
		Summary summary;
		
		hand = new Hand ("2H AS AD 5H 4C");
		summary = hand.getSummary();
		assertTrue (summary.toString().equals("2 of 14, 1 of 5, 1 of 4, 1 of 2"));
		
		hand = new Hand ("2H AS AD 5H 2C");
		summary = hand.getSummary();
		assertTrue (summary.toString().equals("2 of 14, 2 of 2, 1 of 5"));
		
		hand = new Hand ("2H AS 2H AD 2C");
		summary = hand.getSummary();
		assertTrue (summary.toString().equals("3 of 2, 2 of 14"));
	}
	
	@Test
	public void testHasStraightFlush()
	{
		Hand hand;
		
		hand = new Hand ("3H 4H 7H 6H 5H");
		assertTrue ("Has straight flush", hand.hasStraightFlush());
		
		hand = new Hand ("3H 4H 7D 6H 5H");
		assertTrue ("Has no straight flush", !hand.hasStraightFlush());
	}

	@Test
	public void testHasQuads()
	{
		Hand hand;
		
		hand = new Hand ("3H 3D 3C 5H 3S");
		assertTrue ("Has quads", hand.hasQuads());
		
		hand = new Hand ("3H 3D 3C 5S 5H");
		assertTrue ("Has no quads", !hand.hasQuads());
	}

	@Test
	public void testHasTrips()
	{
		Hand hand;
		
		hand = new Hand ("3H 3D 6C 5H 3S");
		assertTrue ("Has trips", hand.hasTrips());
		
		hand = new Hand ("3H 3D 5C 5H 3S");
		assertTrue ("Has no trips", !hand.hasTrips());
		
		hand = new Hand ("3H 2D 6C 5H 3S");
		assertTrue ("Has no trips", !hand.hasTrips());
		
		hand = new Hand ("3H 3D 5C 5H 3S");
		assertTrue ("Has no trips", !hand.hasTrips());
	}
	
	@Test
	public void testHasFullHouse()
	{
		Hand hand;
		
		hand = new Hand ("3H 3D 5C 5H 3S");
		assertTrue ("Has full house", hand.hasFullHouse());
		
		hand = new Hand ("3H 3D 6C 5H 3S");
		assertTrue ("Has no full house", !hand.hasFullHouse());
	}
	
	@Test
	public void testHasTwoPair()
	{
		Hand hand;
		
		hand = new Hand ("3H 3D 5C 5H 4S");
		assertTrue ("Has two pair", hand.hasTwoPair());
		
		hand = new Hand ("3H 3D 5C 5H 3S");
		assertTrue ("Has no two pair", !hand.hasTwoPair());
	}
	
	@Test
	public void testHasOnePair()
	{
		Hand hand;
		
		hand = new Hand ("3H 8D 5C 3H 4S");
		assertTrue ("Has one pair", hand.hasOnePair());
		
		hand = new Hand ("3H 3D 5C 5H 5S");
		assertTrue ("Has no one pair", !hand.hasOnePair());
	}
	
	@Test
	public void testHandType()
	{
		Hand hand1, hand2;
		
		hand1 = new Hand ("3H 3D 3C 5H 3S");
		hand2 = new Hand ("3H 3D 5C 5H 3S");
		assertTrue ("Quads beats full house", hand1.beats(hand2));
		
		hand1 = new Hand ("2H AH AH 5H 4H");
		hand2 = new Hand ("3H 3D 6C 5H 3S");
		assertTrue ("Flush beats trips", hand1.beats(hand2));
		
		hand1 = new Hand ("4H 4D 6C 5H 4S");
		hand2 = new Hand ("3H 3D 6C 5H 3S");
		assertTrue ("Trip 4s beats trip 3s", hand1.beats(hand2));
		
		hand1 = new Hand ("7H 3S 6D 5H 4C");
		hand2 = new Hand ("2H 3S 6D 5H 4C");
		assertTrue ("Straight from 3 beats straight from 2", hand1.beats(hand2));
		
		hand1 = new Hand ("2H 3S 6D 5H 4C");
		hand2 = new Hand ("2H 3S AD 5H 4C");
		assertTrue ("Straight from 2 beats straight from Ace", hand1.beats(hand2));
		
		hand1 = new Hand ("2H 3H 6H 5H 4H");
		hand2 = new Hand ("2C 3C AC 5C 4C");
		assertTrue ("Straight flush from 2 beats straight flush from Ace", hand1.beats(hand2));
		
		hand1 = new Hand ("3H 3D 3C 5H 3S");
		hand2 = new Hand ("2H 2D 2C 5H 2S");
		assertTrue ("Quad 3s beats quad 2s", hand1.beats(hand2));
		
		hand1 = new Hand ("3H 3D 5C 5H 3S");
		hand2 = new Hand ("2H 2D 6C 6H 2S");
		assertTrue ("3s full of 5s beats 2s full of 6s", hand1.beats(hand2));
		
		hand1 = new Hand ("3H AH JH 5H 4H");
		hand2 = new Hand ("2C AC JC 5C 4C");
		assertTrue ("Low 3 flush beats low 2 flush", hand1.beats(hand2));
		
		hand1 = new Hand ("3H 3D 6C 6H 8S");
		hand2 = new Hand ("3H 3D 5C 5H 8S");
		assertTrue ("Two pair 6-3-8 beats two pair 5-3-8", hand1.beats(hand2));
		
		hand1 = new Hand ("4H 4D 5C 5H 8S");
		hand2 = new Hand ("3H 3D 5C 5H 8S");
		assertTrue ("Two pair 5-4-8 beats two pair 5-3-8", hand1.beats(hand2));
		
		hand1 = new Hand ("3H 3D 5C 5H 9S");
		hand2 = new Hand ("3H 3D 5C 5H 8S");
		assertTrue ("Two pair 5-3-9 beats two pair 5-3-8", hand1.beats(hand2));
		
		hand1 = new Hand ("6H 8D 5C 6H 4S");
		hand2 = new Hand ("3H 8D 5C 3H 4S");
		assertTrue ("Pair of 6s beats pair of 3s", hand1.beats(hand2));
		
		hand1 = new Hand ("3H 8D 6C 3H 5S");
		hand2 = new Hand ("3H 8D 6C 3H 4S");
		assertTrue ("Pair with 5 kicker beats 4 kicker", hand1.beats(hand2));
		
		hand1 = new Hand ("4H 8D 6C JH 5S");
		hand2 = new Hand ("3H 8D 6C JH 5S");
		assertTrue ("High card with low 4 beats low 3", hand1.beats(hand2));
	}
}
