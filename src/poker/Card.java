package poker;

public class Card implements Comparable<Card>
{
	public enum Suit
	{
		CLUBS, DIAMONDS, HEARTS, SPADES;
	}

	public final static int JACK = 11;
	public final static int QUEEN = 12;
	public final static int KING = 13;
	public final static int ACE = 14;
	
	private Suit suit;
	private int value;
	
	public Suit getSuit() { return suit; }
	public int getValue() { return value; }

	public Card (String card)
	{
		switch (card.charAt(0))
		{
		case 'A': value = ACE; break;
		case 'K': value = KING; break;
		case 'Q': value = QUEEN; break;
		case 'J': value = JACK; break;
		case 'T': value = 10; break;
		case '9': value = 9; break;
		case '8': value = 8; break;
		case '7': value = 7; break;
		case '6': value = 6; break;
		case '5': value = 5; break;
		case '4': value = 4; break;
		case '3': value = 3; break;
		case '2': value = 2; break;
		}
		
		switch (card.charAt(1))
		{
		case 'C': suit = Suit.CLUBS; break;
		case 'D': suit = Suit.DIAMONDS; break;
		case 'H': suit = Suit.HEARTS; break;
		case 'S': suit = Suit.SPADES; break;
		}
	}
	
	@Override
	public String toString()
	{
		String result;
		
		switch (value)
		{
		case ACE: result = "Ace of "; break;
		case KING: result = "King of "; break;
		case QUEEN: result = "Queen of "; break;
		case JACK: result = "Jack of "; break;
		default: result = String.format ("%d of ", value);
		}
		
		result += suit.toString();
		
		return result;
	}
	
	@Override
	public int compareTo(Card card)
	{
		return Integer.valueOf(card.value).compareTo(this.value);
	}
}
