package poker;

import java.util.Arrays;

public class Hand
{
	public enum HandType
	{
		HIGH_CARD, ONE_PAIR, TWO_PAIR, TRIPS, STRAIGHT, FLUSH, FULL_HOUSE, QUADS, STRAIGHT_FLUSH;
	}
	
	private Card[] Cards;
	
	private HandType Type;
	public HandType getType()
	{
		return Type;
	}

	private Summary Summary;
	public Summary getSummary()
	{
		return Summary;
	}

	/**
	 * Create new hand from text description. Sort hand according to card values (Ace high).
	 * @param hand Format is VS VS VS VS VS, where V is card value and S is card suit.
	 */
	public Hand (String hand)
	{
		Cards = new Card[5];
		for (int n = 0; n <= 4; n++)
		{
			String code = hand.substring(n*3, n*3+2);
			Cards[n] = new Card (code);
		}
		Arrays.sort(Cards);
		
		// Add each card to the hand summary where the total of each value is kept
		this.Summary = new Summary();
		for (Card card: Cards)
			Summary.increment(card.getValue());
		Summary.sort();
		
		setType();
		
		return;
	}
	
	private void setType()
	{
		if (hasStraightFlush()) Type = HandType.STRAIGHT_FLUSH;
		else if (hasQuads()) Type = HandType.QUADS;
		else if (hasFullHouse()) Type = HandType.FULL_HOUSE;
		else if (hasFlush()) Type = HandType.FLUSH;
		else if (hasStraight()) Type = HandType.STRAIGHT;
		else if (hasTrips()) Type = HandType.TRIPS;
		else if (hasTwoPair()) Type = HandType.TWO_PAIR;
		else if (hasOnePair()) Type = HandType.ONE_PAIR;
		else Type = HandType.HIGH_CARD;
	}
	
	public boolean hasCard (Card.Suit suit)
	{
		for (Card card: Cards)
			if (card.getSuit() == suit)
				return true;
		
		return false;
	}
	
	public boolean hasCard (int value)
	{
		for (Card card: Cards)
			if (card.getValue() == value)
				return true;
		
		return false;
	}
	
	public boolean hasFlush()
	{
		// Get suit of first card and check the others are the same
		Card.Suit first = Cards[0].getSuit();
		for (int n = 1; n <= 4; n++)
			if (Cards[n].getSuit() != first)
				return false;
		
		return true;
	}
	
	public boolean hasStraight()
	{
		// Check specifically for Ace to 5 straight
		// Order will be A5432 because of hand sorting order
			if (Cards[0].getValue() == 14 &&
				Cards[1].getValue() == 5 &&
				Cards[2].getValue() == 4 &&
				Cards[3].getValue() == 3 &&
				Cards[4].getValue() == 2)
				return true;
		
		// Check for sequence of five cards starting with value of the highest
		int first = Cards[0].getValue();
		for (int n = 1; n <= 4; n++)
			if (Cards[n].getValue() != --first)
				return false;
		
		return true;
	}
	
	public boolean hasStraightFlush()
	{
		return (hasStraight() && hasFlush());
	}
	
	public boolean hasQuads()
	{
		return (Summary.getTotal(0) == 4 &&
			Summary.getTotal(1) == 1);
	}
	
	public boolean hasTrips()
	{
		return (Summary.getTotal(0) == 3 &&
			Summary.getTotal(1) == 1 &&
			Summary.getTotal(2) == 1);
	}
	
	public boolean hasFullHouse()
	{
		return (Summary.getTotal(0) == 3 &&
			Summary.getTotal(1) == 2);
	}
	
	public boolean hasTwoPair()
	{
		return (Summary.getTotal(0) == 2 &&
			Summary.getTotal(1) == 2 &&
			Summary.getTotal(2) == 1);
	}
	
	public boolean hasOnePair()
	{
		return (Summary.getTotal(0) == 2 &&
			Summary.getTotal(1) == 1 &&
			Summary.getTotal(2) == 1 &&
			Summary.getTotal(3) == 1);
	}
	
	@Override
	public String toString()
	{
		String result = "";
		
		for (int n = 0; n <= 4; n++)
		{
			if (result.length() > 0)
				result += ", ";
			result += Cards[n].toString();
		}
		
		return result;
	}
	
	public boolean beats (Hand target)
	{
		// If the hands are of different types then just compare the types
		if (this.Type != target.Type)
			return this.Type.compareTo(target.Type) > 0;
			
		// Otherwise use type-specific rules
		switch (this.Type)
		{
		case STRAIGHT:
		case STRAIGHT_FLUSH:
			// The straight A5432 beats no other straight
			if (this.Cards[0].getValue() == 14 &&
				this.Cards[4].getValue() == 2)
				return false;
			
			if (target.Cards[0].getValue() == 14 &&
				target.Cards[4].getValue() == 2)
				return true;
			
			// Otherwise compare the top cards
			return this.Cards[0].getValue() > target.Cards[0].getValue();
			
		case FLUSH:
		case HIGH_CARD:
			// Compare cards in order until there's a difference
			for (int n = 0; n <= 4; n++)
				if (this.Cards[n].getValue() != target.Cards[n].getValue())
					return this.Cards[n].getValue() > target.Cards[n].getValue();
					
			return false;
					
		case QUADS:
		case FULL_HOUSE:
		case TRIPS:
			return this.Summary.getValue(0) > target.Summary.getValue(0);
			
		case TWO_PAIR:
			for (int n = 0; n <= 2; n++)
				if (this.Summary.getValue(n) != target.Summary.getValue(n))
					return this.Summary.getValue(n) > target.Summary.getValue(n);
					
			return false;
			
		case ONE_PAIR:
			for (int n = 0; n <= 3; n++)
				if (this.Summary.getValue(n) != target.Summary.getValue(n))
					return this.Summary.getValue(n) > target.Summary.getValue(n);
					
			return false;
		}
			
		return false;
	}
}
