package poker;

/*
 * Represents the total number of cards with a given value
 */
public class Count implements Comparable<Count>
{
	private int Value;
	public int getValue()
	{
		return Value;
	}

	private int Total;
	public int getTotal()
	{
		return Total;
	}
	
	public Count (int value)
	{
		Value = value;
		Total = 1;
	}
	
	public void increment()
	{
		Total++;
	}

	@Override
	public int compareTo(Count target)
	{
		if (this.Total < target.Total)
			return 1;
		if (this.Total > target.Total)
			return -1;
		
		// Totals are equal so now compare Values
		return Integer.valueOf(target.Value).compareTo(this.Value);
	}
}
