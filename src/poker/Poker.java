package poker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Poker
{
	public static void main(String[] args)
	{
		try
		{
			BufferedReader reader = new BufferedReader (new FileReader ("poker.txt"));
			
			String line;
			int hand1Wins = 0, hand2Wins = 0;
			
			while ((line = reader.readLine()) != null)
			{
				Hand hand1 = new Hand (line.substring (0, 14));
				Hand hand2 = new Hand (line.substring (15));
				System.out.println (String.format ("Line: %s", line));
				System.out.println (String.format ("Hand 1: %s", hand1.toString()));
				System.out.println (hand1.getSummary().toString());
				System.out.println (String.format ("Hand 2: %s", hand2.toString()));
				System.out.println (hand2.getSummary().toString());
				
				if (hand1.beats (hand2))
				{
					System.out.println(String.format("HAND 1 WINS! (%s)", hand1.getType().toString()));
					hand1Wins++;
				}
				else
				{
					System.out.println(String.format("HAND 2 WINS! (%s)", hand2.getType().toString()));
					hand2Wins++;
				}
				
				System.out.println();
			}
			
			reader.close();
			
			System.out.println(String.format("Hand 1 wins: %d", hand1Wins));
			System.out.println(String.format("Hand 2 wins: %d", hand2Wins));
		}
		catch (FileNotFoundException e)
		{
			System.out.println (String.format ("Can't find file: %s", "poker.txt"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
