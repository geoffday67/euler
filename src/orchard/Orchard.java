package orchard;

public class Orchard
{
	public static final int HUNDRED_MILLION = 100000000;
	
	public static int countHidden (int side)
	{
		int ncp = side - Factoriser.phi(side);
		return (6 * ncp);
	}
	
	public static long totalHidden (int order)
	{
		long total = 0;
		
		for (int n = 2; n <= order; n++)
		{
			total += countHidden (n);
			
			if (n % 1000000 == 0)
				System.out.println(String.format("%d rows complete", n));
		}
		
		return total;
	}
	
	public static void main(String[] args)
	{
		Factoriser.initialise(HUNDRED_MILLION);

		long begin = System.nanoTime();
		int n = HUNDRED_MILLION;
		System.out.println(String.format("Order: %d, hidden = %d", n, totalHidden(n)));
		long end = System.nanoTime();
		System.out.println(String.format("That took %f seconds", (end - begin) / 1000000000.0));
		
		/*for (int n = 2; n <= 10; n++)
			System.out.println(String.format("Row: %d, hidden = %d", n, countHidden(n)));*/
		
		/*for (int n = 2; n <= 1000; n++)
		{
			System.out.print(String.format("%d : ", n));
			for (Integer i: Factoriser.getFactors(n))
				System.out.print(String.format("%d ", i));
			System.out.println();
		}
		
		for (int n = 99999000; n <= 100000000; n++)
		{
			System.out.print(String.format("%d : ", n));
			for (Integer i: Factoriser.getFactors(n))
				System.out.print(String.format("%d ", i));
			System.out.println();
		}*/
	}
}
