package orchard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

public class Factoriser
{
	private static int[] Primes;
	public static int[] getPrimes() { return Primes; }

	public static int phi (int n)
	{
		int phi = 1;
		int m = 0;
		int previous = 0;

		while (true)
		{
			// If target is (now) prime then add to list and quit.
			// This will happen at some point.
			if (isPrime(n))
			{
				if (n != previous)
					phi *= (n - 1);
				else
					phi *= n;

				return phi;
			}
				
			int p = Primes[m];

			if (n % p == 0)
			{
				n /= p;

				if (p != previous)
				{
					// We have a new prime factor, multiply by p-1
					phi *= (p - 1);
				}
				else
				{
					// We have a repeated prime, multiply by p
					phi *= p;
				}

				previous = p;
			}
			else
				// Try the next prime
				m++;
		}
	}
	
	public static int phiByBits (int n)
	{
		BitSet row = new BitSet (n);
		ArrayList<Integer> factors = Factoriser.getFactors (n);
		for (int factor: factors)
		{
			for (int position = factor; position < n; position += factor)
				row.set (position);
		}
		
		return n - row.cardinality() - 1;
	}

	public static ArrayList<Integer> getFactors(int target)
	{
		int m = 0;
		int previous = 0;
		ArrayList<Integer> factors = new ArrayList<Integer>();

		while (true)
		{
			// If target is (now) prime then add to list and quit.
			// This will happen at some point.
			if (isPrime(target))
			{
				factors.add(target);
				return factors;
			}
				
			int p = Primes[m];

			if (target % p == 0)
			{
				target /= p;

				if (p != previous)
					factors.add(p);

				// Remember this prime so we don't record it again next time round
				previous = p;
			}
			else
				// Try the next prime
				m++;
		}
	}

	public static boolean isPrime(int target)
	{
		return Arrays.binarySearch(Primes, target) >= 0;
	}
	
	public static void initialise(int max)
	{
		calculatePrimes(max);
	}

	private static void calculatePrimes(int max)
	{
		// First create a BitSet with one entry per number.
		// FALSE means number is prime.
		// Create one extra Bitset entry so we can use sieve[1] up to
		// sieve[max].
		BitSet sieve = new BitSet(max + 1);

		// Mark 1 as composite
		sieve.set(1);

		int limit = (int) Math.sqrt((double) max);
		for (int n = 1; n <= limit; n++)
		{
			// If this number is composite then skip it
			if (sieve.get(n))
				continue;

			// It's prime, so mark all its multiples as composite
			for (int m = n * 2; m <= max; m += n)
				sieve.set(m);
		}

		// Count how many prime entries remain
		int count = max - sieve.cardinality();

		// Create an array to hold the primes
		Primes = new int[count];

		// Copy the prime values into the array
		int index = 0;
		for (int n = 1; n <= max; n++)
			if (!sieve.get(n))
				Primes[index++] = n;
	}
}
