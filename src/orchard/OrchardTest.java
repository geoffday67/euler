package orchard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class OrchardTest
{
	@BeforeClass
	public static void calculatePrimes()
	{
		Factoriser.initialise(Orchard.HUNDRED_MILLION);
	}
	
	@Test
	public void testIsPrime()
	{
		int[] prime = {2, 3, 5, 99999989};
		int[] composite = {1, 4, 9, 15, Orchard.HUNDRED_MILLION};
		
		for (int target: prime)
			assertTrue(String.format("Testing %d", target), Factoriser.isPrime(target));
			
		for (int target: composite)
			assertFalse(String.format("Testing %d", target), Factoriser.isPrime(target));
	}
	
	@Test
	public void testGetFactors()
	{
		int[] targets = {2, 60, 99999989};

		@SuppressWarnings("rawtypes")
		ArrayList[] factors = {
			new ArrayList<Integer>(Arrays.asList (2)),
			new ArrayList<Integer>(Arrays.asList (2, 3, 5)),
			new ArrayList<Integer>(Arrays.asList (99999989))
		};
		
		for (int n = 0; n < targets.length; n++)
		{
			ArrayList<Integer> result = Factoriser.getFactors(targets[n]);
			assertEquals(factors[n], result);
		}
	}
	
	@Test
	public void testPhi()
	{
		assertEquals (Factoriser.phi(30), 8);
		assertEquals (Factoriser.phi(450), 120);
		assertEquals (Factoriser.phi(499), 498);
		assertEquals (Factoriser.phi(300), 80);
		assertEquals (Factoriser.phi(2), 1);

		try
		{
			Scanner scanner = new Scanner (new File ("phi.txt"));
			while (scanner.hasNextInt())
			{
				int n = scanner.nextInt();
				int phi = scanner.nextInt();
				
				assertEquals (phi, Factoriser.phi(n));
			}
			scanner.close();
		}
		catch (FileNotFoundException e)
		{
			fail (String.format ("Can't find file: %s", "phi.txt"));
		}
	}
	
	@Test
	public void testPhiByBits()
	{
		assertEquals (Factoriser.phiByBits(30), 8);
		assertEquals (Factoriser.phiByBits(450), 120);
		assertEquals (Factoriser.phiByBits(499), 498);
		assertEquals (Factoriser.phiByBits(2), 1);
	}
	
	@Test
	public void testCountHidden()
	{
		assertEquals (6, Orchard.countHidden(5));
		assertEquals (12, Orchard.countHidden(4));
		assertEquals (6, Orchard.countHidden(3));
		assertEquals (6, Orchard.countHidden(2));
	}
	
	@Test
	public void testCountTotal()
	{
		assertEquals (30, Orchard.totalHidden(5));
		assertEquals (138, Orchard.totalHidden(10));
		assertEquals (1177848, Orchard.totalHidden(1000));
	}
	
	@Ignore
	public void testFactoriseTime ()
	{
		int target = Orchard.HUNDRED_MILLION;

		long begin = System.nanoTime();
		for (int n = 2; n <= target; n++)
			Factoriser.getFactors(n);
		long end = System.nanoTime();
		System.out.println(String.format("testFactorise to %d took %f seconds", target, (end - begin) / 1000000000.0));
		
		assertTrue(true);
	}
	
	@Ignore
	public void testHiddenTime()
	{
		int target = 100000;
		long begin = System.nanoTime();
		System.out.println(String.format("Order: %d, hidden = %d", target, Orchard.totalHidden(target)));
		long end = System.nanoTime();
		System.out.println(String.format("testHidden (%d) took %f minutes", target, (end - begin) / 60000000000.0));
		
		assertTrue(true);
	}
}
