public class collatz
{
	private static long chain_length (long start)
	{
		long term = start;
		long length = 1;
		
		while (term != 1)
		{
			if (term % 2 == 0)
				term = term / 2;
			else
				term = (3 * term) + 1;
			
			length++;
		}
		
		return length;
	}
	
	public static void main(String[] args)
	{
		long max = 0;
		long length, start = 0;
		
		long begin = System.nanoTime();
		for (long n = 1; n <= 1000000; n++)
		{
			length = chain_length (n);
			if (length > max)
			{
				max = length;
				start = n;
			}
		}
		long end = System.nanoTime();
			
		System.out.println (start);
		System.out.format ("That took %f seconds", (end - begin) / 1000000000.0);
	}
}
