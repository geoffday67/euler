public class sum_square_difference
{
	private static long calculate (int limit)
	{
		long total = 0;
		
		for (int n = 1; n <= limit; n++)
			total += (n * n);
		
		long sum = limit * (limit  + 1) / 2;
		return (sum * sum) - total;
	}
	
	public static void main(String[] args)
	{
		System.out.println (calculate(100));
	}
}
