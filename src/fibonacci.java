import java.math.BigInteger;

public class fibonacci
{
	public static void main(String[] args)
	{
		BigInteger a, b, f, limit, total;
		
		a = new BigInteger("1");
		b = new BigInteger("2");
		f = new BigInteger("3");
		limit = new BigInteger("4000000");
		total = new BigInteger("2");
		
		while (f.compareTo(limit) < 0)
		{
			if (!f.testBit(0))
				total = total.add(f);
			//System.out.println(String.format("%d %s", f, f.testBit(0) ? "odd" : "even"));
			a = b;
			b = f;
			f = a.add(b);
		}
		
		System.out.println(total);
	}
}
